cmake_minimum_required(VERSION 3.10)

project(JuliaSet)
add_executable(${PROJECT_NAME} "src/main.cpp" "src/glad/glad.c")

configure_file( "src/quad.vertex" "quad.vertex")
configure_file( "src/quad.fragment" "quad.fragment")

set_property(TARGET JuliaSet PROPERTY CXX_STANDARD 14)
set_property(TARGET JuliaSet PROPERTY CXX_STANDARD_REQUIRED ON)

target_compile_options( JuliaSet PRIVATE -Wall -Wextra)

target_include_directories( JuliaSet PRIVATE src)

target_link_libraries( JuliaSet PRIVATE dl)

find_package (OpenGL REQUIRED)
target_link_libraries( JuliaSet PRIVATE OpenGL::GL)
target_link_libraries( JuliaSet PRIVATE OpenGL::GLU)

find_package (glfw3 REQUIRED)
target_link_libraries( JuliaSet PRIVATE ${GLFW3_LIBRARY})
