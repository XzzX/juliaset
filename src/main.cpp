#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <array>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
   glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window)
{
   if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      glfwSetWindowShouldClose(window, true);
}

int main()
{
   glfwInit();
   glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
   glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
   glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

   GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", nullptr, nullptr);
   if (window == nullptr)
   {
      std::cout << "Failed to create GLFW window" << std::endl;
      glfwTerminate();
      return EXIT_FAILURE;
   }
   glfwMakeContextCurrent(window);
   glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

   if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
   {
      std::cout << "Failed to initialize GLAD" << std::endl;
      return EXIT_FAILURE;
   }

   glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

   std::array<float, 12> vertices = { +0.5f, +0.5f, 0.0f,
                                      +0.5f, -0.5f, 0.0f,
                                      -0.5f, -0.5f, 0.0f,
                                      -0.5f, +0.5f, 0.0f};

   std::array<unsigned int, 6> indices = { 0, 1, 3,
                                           1, 2, 3};

   unsigned int VAO;
   glGenVertexArrays(1, &VAO);
   glBindVertexArray(VAO);

   unsigned int VBO;
   glGenBuffers(1, &VBO);
   glBindBuffer(GL_ARRAY_BUFFER, VBO);
   glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);

   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
   glEnableVertexAttribArray(0);


   unsigned int EBO;
   glGenBuffers(1, &EBO);

   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices[0], GL_STATIC_DRAW);

   glBindVertexArray( 0);
   glBindBuffer(GL_ARRAY_BUFFER, 0);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

   char* cVertex = new char[1024];
   memset(cVertex, '\0', 1024);
   std::ifstream fVertex("quad.vertex", std::ifstream::in);
   fVertex.read(cVertex, 1024);
   fVertex.close();

   unsigned int vertexShader;
   vertexShader = glCreateShader(GL_VERTEX_SHADER);
   glShaderSource(vertexShader, 1, &cVertex, nullptr);
   glCompileShader(vertexShader);
   delete[] cVertex;

   int  success;
   char infoLog[512];

   glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
   if(!success)
   {
      glGetShaderInfoLog(vertexShader, 512, nullptr, infoLog);
      std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
   }

   char* cFragment = new char[1024];
   memset(cFragment, '\0', 1024);
   std::ifstream fFragment("quad.fragment", std::ifstream::in);
   fFragment.read(cFragment, 1024);
   fFragment.close();

   unsigned int fragmentShader;
   fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
   glShaderSource(fragmentShader, 1, &cFragment, nullptr);
   glCompileShader(fragmentShader);
   delete[] cFragment;

   glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
   if(!success)
   {
      glGetShaderInfoLog(fragmentShader, 512, nullptr, infoLog);
      std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
   }

   unsigned int shaderProgram = glCreateProgram();
   glAttachShader(shaderProgram, vertexShader);
   glAttachShader(shaderProgram, fragmentShader);
   glLinkProgram(shaderProgram);

   glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
   if(!success) {
      glGetProgramInfoLog(shaderProgram, 512, nullptr, infoLog);
      std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
   }

   glDeleteShader(vertexShader);
   glDeleteShader(fragmentShader);

   while(!glfwWindowShouldClose(window))
   {
      processInput(window);

      glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT);

      glUseProgram(shaderProgram);
      glBindVertexArray(VAO);
      glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);

      glfwSwapBuffers(window);
      glfwPollEvents();
   }

   glfwTerminate();
   return EXIT_SUCCESS;
}
